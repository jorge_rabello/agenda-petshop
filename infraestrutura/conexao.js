const mysql = require('mysql')

const conexao = mysql.createConnection({
  host: '172.19.0.2',
  port: 3306,
  user: 'root',
  password: 'root',
  database: 'agenda-petshop',
  multipleStatements: true
})

module.exports = conexao
